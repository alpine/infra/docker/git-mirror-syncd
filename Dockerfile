FROM alpine

RUN apk add --no-cache alpine-git-mirror-syncd openssh-client

ENV CONFIG /etc/git-mirror-syncd/config.lua

ENTRYPOINT [ "/usr/bin/git-mirror-syncd" ]
